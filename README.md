# HCL Commerce V9 en Kubernetes

**HCL Commerce** proporciona una plataforma robusta de interacción con el cliente para el comercio omnicanal. Puede ser utilizado por empresas de todos los tamaños. Proporciona herramientas fáciles de usar para que los usuarios empresariales administren de forma centralizada una estrategia multicanal. Los usuarios comerciales pueden crear y administrar campañas de marketing, promociones, catálogos y comercialización en todos los canales de ventas, o utilizar capacidades integradas de administración de contenido habilitadas para IA. HCL Commerce es una plataforma que ofrece la capacidad de hacer negocios directos con los consumidores (B2C) y con las empresas (B2B). HCL Commerce es una solución personalizable, escalable y de alta disponibilidad diseñada para utilizar estándares abiertos. Utiliza tecnología compatible con la nube para que la implementación y el funcionamiento sea fácil y eficiente.

A partir de la versión 9 de HCL Commerce su arquitectura está soportada para hacer desplegada bajo contenedores basados en docker, facilitando de esta manera su puesta en marcha. Lo que se busca en esta entrada es hacer uso de Kubernetes cómo orquestador de contenedores para desplegar un ambiente de authoring con Solr search y Aurora storefront.

**Notas:** 
- Se recomienda hacer uso de este material solo con fines de prueba.
- Los datos alojados en cada uno de los Pods no maneja persistencia.

### ¿Cómo se desplegará?

![Alt text](misc/Commerce9_Kubernetes.png?raw=true "HCL Commerce 9 - Kubernetes")


El despliegue será realizado en un ambiente on-premise haciendo uso de las siguientes herramientas:

- **Vagrant:** herramienta para crear y administrar entornos de máquinas virtuales en entornos locales. fácil de usar y con enfoque de automatización, Vagrant reduce el tiempo de configuración del entorno de desarrollo.
- **Ansible:** Software de automatización de TI radicalmente simple que automatiza el aprovisionamiento en la nube, la administración de la configuración, la implementación de aplicaciones, la orquestación dentro del servicio y muchas otras necesidades de TI.
- **Kubernetes:** Es una plataforma portable y extensible de código abierto para administrar cargas de trabajo y servicios. Kubernetes facilita la automatización y la configuración declarativa.
- **Docker:** Software de TI orientado a la creación y uso de contenedores de Linux®.
- **Helm:** Gestionador de paquetes para Kubernetes de código abierto. Proporciona la capacidad de proveer, compartir y utilizar software desarrollado para Kubernetes.
- **Vault:** Software que se encarga de la administración de información confidencial de secretos. Protege, almacena y controla el acceso a tokens, contraseñas, certificados, claves de cifrado mediante una interfaz de usuario, CLI o API HTTP.
- **NGINX Ingress Controller:** Ingress expone rutas HTTP y HTTPS desde fuera del clúster a servicios dentro del clúster de Kubernetes.


La instalación comprende los siguientes componentes:
- HCL Commerce Enterprise V9.1.1.0
- HCL Commerce Helm-Charts V9.1.1.0
- Kubernetes 18.10
     - 1 Master node
     - 2 Worker nodes
     - Flannel (Pod Networking)
     - Helm 3.3
          - Vault
          - Nginx Ingress

## Requerimientos

Para el despliegue de este ambiente de HCL Commerce V9 es mandatorio cumplir con los siguientes requisitos:

### Hardware
 - RAM: 12 GB
 - CPU: 8 Cores
 - Disk: 70 GB

### Software
 - S.O: Ubuntu 20.04.1 LTS

 - Git 2.25.1

 - Vagrant 2.2.9
     - Plugin vagrant-libvirt
     - Plugin vagrant-hostsupdater
     - Plugin vagrant-disksize

 - Ansible 2.9.12

 - HCL Commerce Enterprise V9.1.1.0
     - Customization_Server_x86-64.tgz  
     - Store_Server_x86-64.tgz        
     - Transaction_Web_Server_x86-64.tgz
     - DB2_Server_x86-64.tgz 
     - Support_Container_x86-64.tgz   
     - Utility_Server_x86-64.tgz
     - Gem_Store_Web_Server_x86-64.tgz  
     - Tooling_Web_Server_x86-64.tgz
     - Search_Server_x86-64.tgz
     - Transaction_Server_x86-64.tgz
     - HCL Commerce Helm Charts 

Los medios de HCL Commerce pueden ser encontrados en el portal: [HCL Software](https://hclsoftware.flexnetoperations.com/flexnet/operationsportal)

## Creación del cluster local usando Vagrant

Clonar el repositorio
```
git clone git@gitlab.com:ebmnetwork/hcl-commerce-v9-kubernetes.git
```

Creación de las máquinas virtuales para cluster de Kubernetes.
```
vagrant up
```

Verificar el estado de la las máquinas virtuales luego de su creación:
```
vagrant status
```
```
nodo1  running (libvirt)
nodo2  running (libvirt)
nodo3  running (libvirt)
```

## Despliegue de HCL Commerce V9

Ajustar la ruta del origen `src=` que contiene la ruta de la descarga de los medios de HCL Commerce: playbooks/commerce_software.yml
```
- name: Copying HCL Software
  copy: src=/path/Download/HCL/ dest=/opt/medioscommerce/
```

El despliegue del ambiente varía según los limites de ancho de banda y la capacidad de procesamiento. (Estimado 2:00 Horas)
```
ansible-playbook playbooks/main.yml
```

Al finalizar la ejecución de los playbooks de ansible se debe ver un resultado similar al siguiente sin errores.
```
PLAY RECAP ****************************************************************************************
control    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
nodo1      : ok=59   changed=37   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
nodo2      : ok=32   changed=21   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
nodo3      : ok=32   changed=21   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

Finalizado los playbooks de ansible el ambiente de commerce tarda alrededor de 15 minutos en inicializarse completamente.

**Nota:** Dado el caso ante la falla de algún playbook de ansible, se recomienda validar el error y reconstruir el ambiente desde cero.

## Validación del despliegue

Acceder al master node del cluster
```
ssh -l root nodo1
```

Validar el estado de los pods, estos representan cada uno de los servicios de HCL commerce.
```
kubectl get pods -o wide
```
```
NAME                                                READY   STATUS      RESTARTS   AGE   IP           NODE
demoqaauthcrs-app-b9645d67f-vjn2j                   1/1     Running     0          16m   10.244.1.5   nodo2
demoqaauthdb-994fcdb5d-vlchq                        1/1     Running     0          16m   10.244.0.4   nodo1
demoqaauthstore-web-7c7444fc58-wvxdv                1/1     Running     3          16m   10.244.2.6   nodo3
demoqaauthts-app-f787bc57b-r2knx                    1/1     Running     1          16m   10.244.0.5   nodo1
demoqaauthts-web-759d764786-s2cgw                   1/1     Running     0          16m   10.244.2.7   nodo3
demoqaauthxc-app-6c475f4db5-stgb8                   1/1     Running     4          16m   10.244.1.4   nodo2
ingress-ingress-nginx-controller-59cbb5c75b-qt75h   1/1     Running     0          48m   10.244.1.2   nodo2
vault-consul-798558d74b-6w5jm                       2/2     Running     0          16m   10.244.2.5   nodo3
wcs-pre-install-demoqaauth-h2dfybldph-b8tnm         0/1     Completed   0          16m   10.244.1.3   nodo2
```
El estado de todos los pods debe estar en estado Running y Ready.

### URLs del ambiente:

Ahora se podrá acceder a las URL del ambiente. 

- [**Aurora Store Front**](https://store.demoqaauth.pragma.com.co/wcs/shop/en/auroraesite)

![Alt text](misc/aurora_store.png?raw=true "Aurora Store")

- [**Management Center:**](https://cmc.demoqaauth.pragma.com.co/lobtools/cmc/ManagementCenter) User: wcsadmin Password: wcs1admin

- [**Organization Admin Console**](https://org.demoqaauth.pragma.com.co/webapp/wcs/orgadmin/servlet/ToolsLogon?XMLFile=buyerconsole.BuyAdminConsoleLogon)

- [**Accelerator**](https://accelerator.demoqaauth.pragma.com.co/webapp/wcs/tools/servlet/ToolsLogon?XMLFile=common.mcLogon)

- [**Commerce Admin Console**](https://admin.demoqaauth.pragma.com.co/webapp/wcs/admin/servlet/ToolsLogon?XMLFile=adminconsole.AdminConsoleLogon)

- [**Vault GUI:**](http://admin.demoqaauth.pragma.com.co:8200/ui/vault/auth?with=token) Token: 992d4a42-3796-d06e-c4f0-16nf8503877d

## Destruir Ambiente

Podemos destruir el ambiente del cluster simplemente haciendo uso del siguiente comando. 

```
vagrant destroy
```

## Referencias

HCL Technologies. (2019). [HCL Commerce product overview.](https://help.hcltechsw.com/commerce/9.1.0/admin/concepts/covoverall.html)

HashiCorp. (2020). [Introduction to Vagrant.](https://www.vagrantup.com/intro)

Red Hat, Inc. (2020). [How Ansible Works.](https://www.ansible.com/overview/how-ansible-works)

The Linux Foundation. (2020). [What is Kubernetes?.](https://kubernetes.io/es/docs/concepts/overview/what-is-kubernetes/)

The Linux Foundation. (2020). [Helm.](https://helm.sh/docs/)

HashiCorp. (2020). [Vault.](https://www.vaultproject.io/)

F5, Inc. (2020). [NGINX Ingress Controller.](https://kubernetes.github.io/ingress-nginx/)

GitHub, Inc. (2020). [Kubernetes-Ansible-CentOS7.](https://github.com/nilan3/kubernetes-ansible-centos)